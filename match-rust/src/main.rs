fn main() {
    let number = 13;
    println!("Tell me about {}", number);
    match number {
        1 => println!("One!"),
        2 | 3 | 5 | 7 | 11 => println!("This is a prime"),
        13..=19 => println!("A teen"),
        _ => println!("Ain't special"),
    }
    let boolean = true;
    let binary = match boolean {
        false => 0,
        true => 1,
    };
    println!("{} -> {}", boolean, binary);
    for number in 1..=10 {
        match (number % 3, number % 5) {
            (0, 0) => println!("Fizz buzz"),
            (0, _) => println!("Fizz"),
            (_, 0) => println!("Buzz"),
            // (_,_)=>println!("Default {}",number)
            _ => println!("Default {}", number),
        }
    }

    // Enum -> CamelCase
    enum Response {
        Success,
        Error(u32, String),
    };
    let response = Response::Error(200, String::from("asdasdasd"));
    match response {
        Response::Success => println!("Success"),
        Response::Error(404, _) => println!("Error client"),
        Response::Error(500, _) => println!("Error server"),
        Response::Error(_, message) => println!("{}", message),
        _ => println!("asdasdas"),
    };
    response_client_void();
    let sum = sum(1, 2);
    println!("{}", sum);
    let n = 5;
    let f = fib(n);
    println!("fibonacci sum {} {}", n, f);
    let fac = factorial(5);
    println!("{}", fac);
}

fn response_client_void() {
    println!("response");
}

fn sum(number_1: i32, number_2: i32) -> i32 {
    return number_1 + number_2;
}
fn fib(n: u64) -> u64 {
    match n {
        1 | 2 => n - 1,
        n => fib(n - 1) + fib(n - 2),
        _ => 0,
    }
}

fn factorial(number: u32) -> u32 {
    match number {
        0 | 1 => 1,
        number => factorial(number - 1) * number,
    }
}
