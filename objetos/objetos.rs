
struct Triangulo {
    base: f64,
    altura : f64
}
// impl Triangulo {
    // fn area(&mut self)->f64{
    //     (self.base * self.altura)/2.0
    // }
// }
impl Figura for Triangulo {
    fn area(&mut self)->f64{
        (self.base * self.altura)/2.0
    }
}

impl Triangulo {
    fn set_base(&mut self,new_base:f64){
        self.base = new_base;
    }
}

trait Figura {
    fn area(&mut self)->f64;
}

fn main(){
    let mut triangulo = Triangulo {
        base : 10.0,
        altura : 20.0
    };
    let area = triangulo.area();
    println!("{}",area);
    triangulo.set_base(12.0);
    let area = triangulo.area();
    println!("{}",area);
}