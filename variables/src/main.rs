fn main() {
    // println!("Hello, world! con solo hacer cargo run");
    let number_one = 1;
    let number_2 = 10;
    let result = number_one + number_2;
    println!("el resultado es {}", result);
    println!(
        "el resultado de la suma de {} + {} es {}",
        number_one, number_2, result
    );

    // por defecto las variables son inmutables
    // lo que quiere decir que no se puden cambiar en tiempo de ejecución (iguales a una constante)
    // pero si le colocamos la palabra reservada mut se vuelve mutable es decir que puede cambiar en tiempo
    // de ejecucion, el compilador entendera que sea variable es mutable
    let mut number_3: i32 = 2; // numero entero de 32 bits
    println!("{}", number_3);
    number_3 = 12;
    println!("{}", number_3);
    // constantes ==> const o static
    // rust me obliga a que las constantes sean en mayuscular y que no contengan dígitos
    const NUMBER_FOUR: i32 = 4;
    static NUMBER_FIVE: i32 = 5;
    println!("{}", NUMBER_FOUR);
    println!("{}", NUMBER_FIVE);
}
