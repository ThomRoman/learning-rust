# **Learning Rust (curso CodigoFacilito)**

Es un lenguaje desarrollado por los ingenieros de Mozila.
Al igual que c, java y c++ , rust es un lenguaje compilado.

Rust no cuenta con el Garbage Collector eso quiero decir que nosotros somos los reponsables
del uso de memoria.

Rust posee su propio manejador de paquetes al igual que node -> npm
java -> (maven || gradle)  y rust -> cargo

Rust es un lenguaje de bajo nivel una sintaxis moderna

Encargado en la programación de sistemas

> (Raise Condition)

## **Installation**

```sh
    curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```

![Installation](img/installation.png)

![Options](img/options.png)

![1](img/1.png)

![enable-cargo](img/enable-cargo.png)

```sh
    vim ~/.zshrc
    # vim
    export PATH=$HOME/.cargo/env:$PATH
    source ~/.zshrc
```

```sh
    rustup --version
```

```sh
    rustc hello.rs
    ./hello
```

## **Cargo**

- Podemos implementar dependencias de terceros a nuestros proyectos
- Podemos administrar nuestros proyectos
  - Para proyectos grandes es mejor usar cargo y que este se encarge de la administración

```sh
    cargo new variables
    cd variables
    cargo build
    #./target/debug/variables
    cargo run
```

## **Macros**

- [Macros I](https://sodocumentation.net/es/rust/topic/1031/macros)
- [Macros II](https://doc.rust-lang.org/1.7.0/book/macros.html)
- [Macros III](https://hub.packtpub.com/creating-macros-in-rust-tutorial/)
- [Macros IV](http://danigm.net/rust-macros.html)

## **Manejo de memoria**

El stack y el heap son abstracciones que nos permiten comprender en que parte de la memoria
se encuentran almacenadas nuestras variables

### **Stack**

Se encuentran alojadas todas aquellas variables las cuales de ante mano ya conoscamos su tamaño

```rs

    fn main(){
        // nueva variable
        // la variable posee un tamaño de 32 bytes
        let mut x: i32 = 10;
    }
```

El stack tiene la ventaja de ser muy rapido ya que al conecer el tamaño de todas la variables, esta asignará
un espacio en memoria mas optimo para la variable.

Este stack un estructura de grafos de forma de LIFO (last in , firt out), estp hace al stack una estructura 
muy rapida en cuanto lectura y escritura de valores

> [***Recomendacion de mi repositorio de Estructura de Datos***](https://github.com/MaurickThom/EstructuraDeDatos)

```rs
    fn foo(){
        let b = 10; // segunda en apilarse al stack
        let c = 15; // tercera en apilarse al stack
    }

    fn main(){
        let a = 5; // primera en apilarse al stack
        foo();// cuando esta funcion finaliza las variables creadas en su scope seran destruidas y removidas
    }

    /*
    | 15 |
    | 10 |
    | 5  |
    -----
    */

```

### **Heap**

En este caso se agregan aquí cuando a las variables se le desconoce su tamaño
<br>En esta parte encontraremos las variables que puedan variar sus tamaños en tiempo de ejecucion ya sea
que incremente o que decresca, un buen ejemplo a esto son los vectores y los strings, estructuras que como
sabemos son mutables

A diferencia que el stack , el heap es mucho mas lento en lectura como a la escritura de variable

```rs
    fn main(){
        // str ==> stack (inmutable)
        // String ==> heap (mutable)
        let string_stack:str = "estoy en el stack";
        let string_heap :String = String::from("estoy en el heap");
        string_heap.push('a'); // caracter
        string_heap.push('a');
        string_heap.push_str("aaaaaa"); // string

        let new_string = "sdadasd".to_string(); // tipo str -> stack
        let igual:bool = new_string == "sdadasd...".to_string(); // true o false
    }
```

[*Node && Rust*](https://dev.to/gruberb/intro-to-web-programming-in-rust-for-nodejs-developers-lp)
[*API Rust*](https://medium.com/sean3z/building-a-restful-crud-api-with-rust-1867308352d8)
