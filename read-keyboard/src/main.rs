// use std::io;
// use std::io::{stdin};
use std::io::stdin;
fn main() {
    println!("Ingresa el nombre de usuario : ");
    let mut username = String::new();
    // read_line retorna un Result esto retorna un valor de exito o un valor de error
    match stdin().read_line(&mut username) {
        Ok(bytes_length) => {
            println!("{} bytes read", bytes_length);
            let username = username.trim();
            println!("el nombre de usuario es {}", username);
        }
        Err(error) => println!("error: {}", error),
    };
    // io::stdin().read_line(&mut username); // permiso de no solo leer username pasandole la referencia
    // sino tambien decirle que lo mute (modifique)

    println!("ingresa la edad : ");
    let mut age = String::new();
    stdin().read_line(&mut age);
    let age: i32 = age.trim().parse().unwrap();
    println!("La edad es {}", age);
}
