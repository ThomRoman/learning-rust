/*
enum Result<T,E>{ son excluyentes es decir que no se puede tener los dos al mismo tiempo
    // T hace referencia al dato
    // E al error
    Ok(T),
    Err(E)
}
y posee los mismos metodos que el Options
unwrap
unwrap_or
expect
*/
enum ErrorDivision {
    DivisionPorCero,
    DivisionNegativos,
}
fn division(number_1: i32, number_2: i32) -> Result<i32, ErrorDivision> {
    if number_2 == 0 {
        return Err(ErrorDivision::DivisionPorCero);
    }
    if number_1 < 0 || number_2 < 0 {
        return Err(ErrorDivision::DivisionNegativos);
    }
    return Ok(number_1 / number_2);
}
fn main() {
    println!("Hello, world!");
    match division(10, 0) {
        Ok(data) => println!("{}",data),
        Err(ErrorDivision::DivisionNegativos) => println!("Division negativos"),
        Err(ErrorDivision::DivisionPorCero) => println!("Division por cero"),
    };
}
