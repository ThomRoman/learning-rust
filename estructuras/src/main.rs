struct User {
    // convension structura CamelCase
    username: String,
    password: String,
}

fn create_user(username: String, password: String) -> User {
    User { username, password }
}

fn main() {
    let user = User {
        username: String::from("thom"),
        password: String::from("thom123"),
    }; // generando una instancia
    println!(
        "el username es {} y la contraseña es {}",
        user.username, user.password
    );

    let new_username = String::from("new username");
    let new_password = String::from("new password");
    let mut new_user = create_user(new_username, new_password);
    new_user.username = String::from("cambio de username");
    println!(
        "el username es {} y la contraseña es {}",
        new_user.username, new_user.password
    );
}
