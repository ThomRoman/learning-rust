fn quick_sort(vector: Vec<i32>) -> Vec<i32> {
    if vector.len() < 2 {
        return vector;
    }
    let pivot: i32 = vector[0];
    let mut less = Vec::<i32>::new();
    let mut equals = Vec::<i32>::new();
    let mut greater = Vec::<i32>::new();
    for index in 1..vector.len() {
        if vector[index] <= pivot {
            less.push(vector[index]);
        } else {
            greater.push(vector[index]);
        }
    }
    equals.push(pivot);
    
    let mut final_vector = Vec::<i32>::new();
    final_vector.extend(quick_sort(less));
    final_vector.extend(equals);
    final_vector.extend(quick_sort(greater));
    return final_vector;
}

fn main() {
    let vector = vec![12, 24, 4, 45, 7, 3, 28, 4, 2, 4, -1];

    let result = quick_sort(vector);
    println!("{:?}", result);
    // println!("{:?}", vector);
}
