fn main() {
    let _number_uno: i8 = -10;
    let _number_dos: u8 = 10;
    let _caracter: char = 'a';
    let _flot = 12.5; // f32 ,f64
    let _flot64: f64 = 12.3;
    let _bool: bool = true;

    // Operadores aritmenticos
    let num_1: i32 = 10;
    let num_2: i32 = 103;
    let result: f32 = (num_1) as f32 / (num_2) as f32;
    println!("{}", result);
    let result = f64::from(num_2) / f64::from(num_1);
    println!("{}", result);

    // operadores relaciones (comparacion) < > >= <= ==
    // operadores logicos && ||

    // arreglos son estaticos
    let mut arr_numbers: [i32; 4] = [1, 2, 3, 4];
    println!("{:#?}", arr_numbers);
    println!("{:?}", arr_numbers);
    for number in &mut arr_numbers {
        *number = 2;
        println!("{}", number);
    }

    for index in 0..arr_numbers.len() {
        arr_numbers[index] = 4;
        println!("{}", arr_numbers[index]);
    }
    let _create_array = [1; 10]; // 1 1 1 1 ..1  = 10 veces
                                 // vectores son dinamicos
    let mut v: Vec<i32> = vec![1, 2, 3];
    let mut vec: Vec<i32> = Vec::new(); // []
    vec.push(1);

    for (index, value) in vec.iter().enumerate() {
        println!("index {}  value {} ", index, value);
    }

    for number in vec.iter_mut() {
        *number = *number + 1;
    }
    println!("vector {:?}", v);
    println!("vector {:?}", vec);
    v.push(10);
    v.insert(0, 1000); // indice 0 => 1000
    let _end = v.pop().unwrap();
    println!("vector {:?}", v);
    for value in v.iter() {
        println!("{}", value);
    }
    for number in &mut v {
        *number = 10;
        println!("{}", number);
    }
    for index in 0..v.len() {
        v[index] = 0;
        println!("{}", v[index]);
    }
    // las tuplas pueden almacenas diferentes tipos de datos
    let mut tupla: (i32, i32, bool, f64) = (1, 2, true, 1.2);
    println!("{:?}", tupla);
    let (first, ..) = tupla;
    let (.., end) = tupla;
    let f = tupla.0;
    println!("{:?}", tupla);
    tupla.2 = false;
    println!("{}", first);
    println!("{}", f);
    println!("{}", end);
    println!("{:?}", tupla);
}

/*
    rust es un lenguaje fuertemente tipado o estaticamente tipado

    i32 => entero de 32 bits
    18,i16,i64,i128 con signo + y -
    y sin signo u8,u16,u32,u64,u128

    char : carater UTF-8

    valorres flotantes

    Macros :
        Un macro no es más que una "función" que define cómo se generará código,
        el compilador es el encargo del orden de la ejecucion de todas las instrucciones
        en este caso los macros se ejecutan en la primera fase de compilación.
    
    http://danigm.net/rust-macros.html
    http://www.dccia.ua.es/dccia/inf/asignaturas/LPP/2010-2011/teoria/tema5.html
*/
