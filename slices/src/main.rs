fn main() {
    // slices => heap
    // vector ==> heap
    // array ==> stack

    let message: String = String::from("Hello world !!");
    let substring_1_slice = &message[1..4];
    let substring_2_slice = &message[..6];
    let substring_3_slice = &message[2..];

    println!("message {}", message);
    println!("substring_1_slice {}", substring_1_slice);
    println!("substring_2_slice {}", substring_2_slice);
    println!("substring_3_slice {}", substring_3_slice);

    let title = title(&"message".to_string());
    println!("{}", title);

    // ciclo de vida

    let mut string = String::from("nuevo string");
    {
        let prestamo = &string;
        // string = String::from("Cambio de valor"); error por freezing
        println!("prestamo {}", prestamo);
        // freezing es decir la variable string se congela hasta que el bloque termine
    }
    println!("string {}", string);
    // string = String::from("new Cambio de valor");
    // println!("string {}", string);

    println!("mensaje 1");
    println!("mensaje 2");
    println!("mensaje 3");

    panic!("ocurrio algo todo termina aquí");

    println!("mensaje 4");
    println!("mensaje 5");
}

fn title(message: &String) -> String {
    let mut first_letter = message[0..1].to_uppercase();
    first_letter.push_str(&message[1..]);
    return first_letter.to_string();
}
