/*
En rust no existe el null el nil o el undefined
y no hay excepciones

rust implementa dos tipos especiales de enums
Options : nos permitirá conocer si existe o no algun valor
Results : nos permitira trabajar con errores -> panic!

enum Option<T>{
    Some(T),-> El valor
    None -> la ausencia de algún valor
}
*/

#[derive(Debug)]
struct User {
    username: String,
    password: String,
    email: String,
    edad: Option<u32>,
}
// le decir que PUEDE retornar un valor
fn get_value(flag: bool) -> Option<String> {
    match flag {
        true => Some(String::from("Soy un mensaje para la tupla some!")),
        false => None,
    }
}

fn main() {
    let result = get_value(true); // Option
    match &result {
        Some(data) => println!("{}", data),
        None => println!("ningun valor"),
    }

    let user = User {
        username: "Thom".to_string(),
        password: "password".to_string(),
        email: "thomtwd@gmail.com".to_string(),
        edad: None,
    };

    match user.edad {
        Some(edad) => println!("{}", edad),
        None => {}
    };
    println!("{:?}", user);
    // obtener el valor de un option con unwrap este metodo intenta obtener lo que la tupla some
    // almacena
    let value = result.unwrap();// intenta obtener lo que la tupla some almacena en el caso
    // no almacena nada se ejecutará el macro panic!
    /*
        existe un metodo llamado unwrap_or(other_value) intentará obtener valor alguo
        en el caso que en la tupla no exista nada se le asignara un valor por defecto

        expect(message) intentara obtener valor alguno
        en el caso que en la tupla no exista nada se ejecuta el panic! con un mensaje que nosotros
        le colocaremos
    */
    println!("value {}", value);

    let message = Some("Hello world"); // type str
    match message {
        Some("hello world") => println!("hello world"),
        Some("hello world...") => println!("hello world..."),
        Some(_) => println!("Es otro mensaje"),
        None => println!("No existe valor"),
    };
}
