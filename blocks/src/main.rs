use std::io;
fn main() {
    let x: i32 = 4;
    if x == 4 {
        println!("x is four");
    } else if x == 3 {
        println!("x is three");
    } else {
        println!("x is something else");
    }
    // let string = String::from("asdada");
    let string = if 12 * 5 > 54 { "si" } else { "no" };
    println!("{}", string);
    let mut color = String::new();
    io::stdin().read_line(&mut color);
    let color = color.trim().to_lowercase();
    println!("{}", color);

    // Los bloques en rust no es mas que una coleccion de sentencias agrupadas dentro de un juego de llaves {}
    {
        // el shadowing respeta el alcanze de las variables es decir que este bloque al
        // rescribir una variable que esta anteriormente declarada
        // es totalmente diferente por lo que no altera en nada y no destruye la variable
        let m = 3;
        let x = 3;
        println!("{}", x);
        println!("{}", m);
    };
    println!("{}", x);
    // println!("{}",m); error

    // loop

    let mut num = 0i32; // 0 i32
    loop {
        // Infinite loop
        num += 1;
        if num == 3 {
            println!("three");
            continue;
        }
        println!("{}", num);

        if num == 5 {
            println!("OK, that's enough");
            break;
        }
    }
}
