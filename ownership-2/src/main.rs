// #[derive(Debug)]
// #[derive(Debug)] // le decirmos que podemos examinar objetos y se coloca sobre la estructura
// {:?}

struct Rect {
    width: u32,
    height: u32,
}

#[derive(Debug)]
struct User {
    username: String,
    password: String,
}

// implementando metodos a nuestras estructuras
impl User {
    fn saludar(&mut self) {
        //self hace referencia al objeto User
        println!("Hola soy el usuario {}", self.username);
    }
    fn change_password(&mut self, new_password: String) {
        self.password = new_password;
    }
}

// estructura tipo tupla
#[derive(Debug)]
struct Color(
    // RGB
    u32,
    u32,
    u32,
);

fn main() {
    // se respeta la primera regla
    let rect = Rect {
        width: 10,
        height: 20,
    };
    // se rompe la segunda regla
    // ya que su ownership ahora le pertenece a rect_2 y rect se descarta y ahora todo le pertenece a rect_2
    // let rect_2 = rect;
    // rect.width = 19;
    // la cuestion del ownersship solo funciona para valores que se almacenan en el heap

    let rect_2 = &rect;
    println!("{}", rect.width);
    println!("{}", rect.height);
    println!("{}", rect_2.width);
    println!("{}", rect_2.height);

    let mut user = User {
        username: String::from("username"),
        password: String::from("password"),
    };
    user.saludar();
    user.change_password("new password".to_string());
    println!("{:?}", user);
    println!("{}", user.password);

    // estructuras de tipo tupla
    let black = Color(0, 0, 0);
    let white = Color(255, 255, 255);

    let mut custom_color = Color(2, 23, 45);

    custom_color.1 = custom_color.2 + 3;

    println!("{:?}", black);
    println!("{:?}", white);
    println!("{:?}", custom_color);
}
