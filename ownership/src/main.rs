struct Rect {
    width: u32,
    height: u32,
}

// fn area(rect: Rect) -> u32 {
//     rect.width * rect.height
// }
fn area(rect: &Rect) -> u32 {
    rect.width * rect.height
}

fn main() {
    // ownership
    /*
    Es una caracterisca exclusiva de rust y le permite al lenguaje garantizar la seguridad de la
    memoria sin necesidad de utilizar un recolector de basura (Garbage Collector)

    Posee tres reglas :

    1 cada valor en rust tiene su propio ownership
    2 solo puede existir un ownership a la vez.
    3 si un ownership sale del alcance el valor se descartará

    */
    /*
    Aquí si se cumple la primera regla y tambien la segunda
    ya que se le esta asignado la instancia a la variable rec y solo a ella y no comparte su referencia
    let rec = Rect {
        width: 10,
        height: 20,
    };
    // es aqui donde se rompe la tercera regla ya que esta compartiendo su referencia con otra variable
    que es area y ademas entra a un funcion donde su alcance es otro
    let area = area(rec);
    println!("{}", area);
    println!("{}", rec.width);
    println!("{}", rec.height);

    En rust todos los argumentos que utilicemos en funciones o metodos se hacen por prestamos
    es decir la variable el ownership se mueve(estamos sedieno el ownership de nuestras variables)
    esto es el comportamiento por default

    pero podemos modificarlos y solo pasarlos por referencias , es decir una ves en la funcion termine
    que nos devuielva la referencia del ownership prestado

    entonces loq ue sucede es cuando se pasa rec en la funcion area se presta su refencia
    por lo que ahora la variable rec pertenece al scope area y en el scope main ya no

    */

    let rect = Rect {
        width: 10,
        height: 20,
    };

    let area = area(&rect);
    println!("{}", area);
    println!("{}", rect.width);
    println!("{}", rect.height);
}
