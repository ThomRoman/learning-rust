/*
    SHADOWING
        este concepto dicta que podemos usar el nombre de la varaible n numero de veces
        que nosotros necesitemos

*/
fn main() {
    let value: i32 = 10;
    println!("{}", value); // funcion macro println!
    println!("{:p}", &value); // funcion macro println!

    // shadowing
    // lo que sucede es destruir la variable anterior de esta forma se libera el nombre de la variable
    let value = 20;
    println!("{}", value); // funcion macro println!
    println!("{:p}", &value); // funcion macro println!
    let value = "asdasd";
    println!("{}", value); // funcion macro println!
    println!("{:p}", &value); // funcion macro println!
    let value = true;
    println!("{}", value); // funcion macro println!
    println!("{:p}", &value); // funcion macro println!
}
